from argparse import ArgumentParser
from pankmer import PKResults
from pankmer.saturation import sat_plot
import numpy as np
import pandas as pd


def parse_arguments():
    parser = ArgumentParser(description='make a saturation plot empirically')
    parser.add_argument('index')
    parser.add_argument('output')
    parser.add_argument('table')
    return parser.parse_args()

def main():
    args = parse_arguments()
    pk_results = PKResults(args.index)
    total_counts = np.zeros((pk_results.number_of_genomes))
    core_counts = np.zeros((pk_results.number_of_genomes))
    n_genomes = range(1, pk_results.number_of_genomes+1)
    for _, score in pk_results:
        parsed_score = tuple(int(b) for b in f"{int.from_bytes(score, byteorder='big'):0{pk_results.number_of_genomes}b}")
        for i, x in enumerate(parsed_score):
            if x:
                total_counts[i:] +=1
                if i == 0:
                    core_counts[:] +=1
                break
        for i, x in enumerate(parsed_score):
            if i == 0 and not x:
                break
            elif not x:
                core_counts[i:] -= 1
                break
    plotting_data = pd.concat((
        pd.DataFrame({'n_genomes': n_genomes, 'n_kmers': total_counts, 'sequence': ('total',)*pk_results.number_of_genomes}),
        pd.DataFrame({'n_genomes': n_genomes, 'n_kmers': core_counts, 'sequence': ('core',)*pk_results.number_of_genomes})))
    sat_plot(plotting_data, args.output)
    plotting_data.to_csv(args.table)

if __name__ == '__main__':
    main()
